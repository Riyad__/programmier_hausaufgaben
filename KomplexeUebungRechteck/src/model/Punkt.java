package model;



public class Punkt {
 
 // Anfang Attribute
 private int x;
 private int y;
 // Ende Attribute
 
 public Punkt() {
   this.x = 0;
   this.y = 0;
 }

 public Punkt(int x, int y) {
   this.x = x;
   this.y = y;
 }

 // Anfang Methoden
 public int getX() {
   return x;
 }

 public int getY() {
   return y;
 }

 public void setX(int xNeu) {
   x = xNeu;
 }

 public void setY(int yNeu) {
   y = yNeu;
 }

 public boolean equals(Punkt p) {
		return this.x == p.getX() && this.y == p.getY();
   
 }

@Override
public String toString() {
	return "Punkt [x=" + x + ", y=" + y + "]";
}
 

 // Ende Methoden
} // end of Punkt
