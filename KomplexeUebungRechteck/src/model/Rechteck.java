package model;

import java.util.Random;


import com.sun.prism.paint.Color;

public class Rechteck {

	// Anfang Attribute
	private Punkt p;
	private int breite;
	private int hoehe;
	static Random rechteck = new Random();
	static int MAXBREITE = 1200;
	static int MAXHOEHE = 1000;
	// Ende Attribute

	public Rechteck() {
		this.p = new Punkt();
		this.breite = 0;

		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.p = new Punkt(x, y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	// Anfang Methoden
	public int getX() {
		return this.p.getX();
	}

	public void setX(int xNeu) {
		this.p.setX(xNeu);
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int yNeu) {
		this.p.setX(yNeu);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);

	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);

	}

	public boolean enthaelt(int x, int y) {
		return this.p.getX() <= x && x <= this.p.getX() + this.breite && this.p.getY() <= y
				&& y <= this.p.getY() + this.hoehe;
	}

	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}

	public boolean enthaelt(Rechteck rechteck) {
		Punkt punkt1 = new Punkt(rechteck.getX(), rechteck.getY());
		Punkt punkt2 = new Punkt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
		return enthaelt(punkt1) && enthaelt(punkt2);
	}

	public static Rechteck generiereZufallsRechteck() {
		int x = rechteck.nextInt(MAXBREITE );
		int y = rechteck.nextInt(MAXHOEHE + 1);
		int breite = rechteck.nextInt(MAXBREITE - x );
		int hoehe = rechteck.nextInt(MAXHOEHE - y );
		return new Rechteck(x, y, breite, hoehe);
	}

	@Override
	public String toString() {
		return "Rechteck [p=" + p + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}


	// Ende Methoden
} // end of Rechteck