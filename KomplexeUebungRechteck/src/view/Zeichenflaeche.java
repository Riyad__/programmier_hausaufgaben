package view;
import java.awt.Color;

import java.awt.Graphics;
import javax.swing.JPanel;
import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")

public class Zeichenflaeche extends JPanel {

    private BunteRechteckeController brc;

    public Zeichenflaeche(BunteRechteckeController brc) {
        this.brc = brc;
    }

    public void paintComponent(Graphics Rechteck) {
        Rechteck.setColor(Color.BLACK);
        for (Rechteck r : brc.getRechtecke()) {
            Rechteck.drawRect(r.getX(), r.getY(), r.getBreite(), r.getHoehe());
        }
    }
}