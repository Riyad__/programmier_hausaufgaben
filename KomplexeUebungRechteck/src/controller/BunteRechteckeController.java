package controller;

import java.util.LinkedList;
import java.util.List;
import model.Rechteck;

public class BunteRechteckeController {

	private List<Rechteck> rechtecke;

	public static void main(String[] args) {
	}

	public BunteRechteckeController() {
	
		 rechtecke = new LinkedList<Rechteck>();

	}

	public void add(Rechteck rechteck) {

		rechtecke.add(rechteck);
	}

	public void reset() {
		rechtecke.clear();
	}

	public List<Rechteck> getRechtecke() {
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}
	 public void generiereZufallsRechtecke(int anzahl) {
		 this.reset();
		 for(int i = 0; i < anzahl; i++)
				rechtecke.add(Rechteck.generiereZufallsRechteck());
	 }

	@Override
	public String toString() {
		return "\nBunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

}
